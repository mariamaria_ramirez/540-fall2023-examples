'''
Example from https://www.digitalocean.com/community/tutorials/python-socket-programming-server-client
'''

import socket


def server_program():
    # get the hostname (if client and server is in the same Host for testing purposes)
    host = socket.gethostname()
    #If you connect from a different host, change to your IP (or hostname if you are using DNS)
    #host = '192.168.0.132'
    
    port = 15789  # initiate port number above 1024

    '''
    If server is behind a firewall, you need to add the port, example with ufw:
    sudo ufw allow 15789
    '''
    
    server_socket = socket.socket()  # get instance
    # look closely. The bind() function takes tuple as argument
    server_socket.bind((host, port))  # bind host address and port together

    # configure how many client the server can listen simultaneously
    server_socket.listen(2)
    client, address = server_socket.accept()  # accept new connection
    client.send(f"I am the server accepting connections on port {port}...".encode())
    print("Connection from: " + str(address))
    
    while True:
        # receive data stream. it won't accept data packet greater than 1024 bytes
        data = client.recv(1024).decode("utf-8")
        if not data:
            # if data is not received break
            break
        print("from connected user: " + str(data))
        data = input(' -> ')
        #Your challenge:
        #We want to change the chat behaviour for having multiple clients requesting to the server
        #For example, requesting an authentication token after validation of user and password.
        #So, we dont input, we just sent back a message to a client
        #Example, to send an authentication token
        #data = f'Here you need to send the authentication token'
        client.send(data.encode("utf-8"))  # send data to the client

    client.close()  # close the connection


if __name__ == '__main__':
    server_program()